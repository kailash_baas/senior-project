import Image

import numpy as np

def load_data(image_path):
    """
    Return a list of the grayscaled pixels of the target image
    """
    imagefile = Image.open(image_path)
    pixelObj = imagefile.load()
    grayscale_list = []

    for i in range(28):
        for j in range(28):
            grayscale_list.append(grayscale(pixelObj[i,j]))
    
    return grayscale_list

def grayscale(pixel):
    """
    @param pixel a pixel object representing a pixel
    Converts the rgba value of the pixel to the equivalent grayscale value.
    Ignores the alpha value when converting.

    >>> imagefile = Image.open("path to some image")
    >>> pixelObj = imagefile.load()
    >>> pixel = pixelObj[0,0] # accessing the pixel at 0,0
    >>> grayscale(pixel)
    """
    return 0.2989 * pixel[0] + 0.5870 * pixel[1] + 0.1140 * pixel[2]

def load_data_wrapper(image_path):
    """
    Returns a numpy array of dimensions (784, 1) of the grayscaled image ready
    for input into the neural network.
    """
    data = load_data(image_path)
    data_inputs = [x / 255 for x in data]
    np_data = np.reshape(data_inputs, (784, 1))
    return np_data
