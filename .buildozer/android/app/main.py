__version__ = '0.1'

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from plyer import camera
import network

class UI(FloatLayout):
    def __init__(self, **kwargs):
        super(UI, self).__init__(**kwargs)
        self.lblCam = Label(text="Click to take a picture! One digit at a time")
        self.add_widget(self.lblCam)

    def on_touch_down(self, e):
        camera.take_picture('/storage/extSdCard/DCIM/Camera/image.jpg', self.done)

    def done(self, e):
        self.lblCam.txt = network.run_network(e)

class Camera(App):
    def build(self):
        ui = UI()
        return ui

    def on_pause(self):
        return True

    def on_resume(self):
        pass

Camera(resolution=(28, 28)).run()
